#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import pymol
from biopandas.pdb import PandasPdb

class wnDirectorio():
    def __init__(self):
        self.constructor = Gtk.Builder()
        self.constructor.add_from_file("./VentanaDirectorio.ui")

        self.window = self.constructor.get_object("VentanaDirectorio")
        self.window.connect("destroy", Gtk.main_quit)
        self.window.set_title("Directorio")
        self.window.show_all()

        botonAceptar = self.constructor.get_object("botonAceptar")
        botonAceptar.connect("clicked", self.botonPDB)

        botonCancelar = self.constructor.get_object("botonCancelar")
        botonCancelar.connect("clicked", self.botonCerrar)


    def botonPDB(self, btn=None):
        self.archivo = self.window.get_file()
        self.nombreArchivo = self.archivo.get_basename()
        print(self.nombreArchivo)
        
        self.ruta = self.window.get_current_folder()
        print(self.ruta)

        # Información PDB
        ppdb = PandasPdb()
        ppdb.read_pdb(self.ruta + "/" + self.nombreArchivo)
        for i in ppdb.df.keys():
            print (i)
            print(ppdb.df[i])
            print("\n")
            
            if (i != "ANISOU"): # PROBLEMA, se generan todos los archivos pero no el de ANISOU
                nom = self.nombreArchivo + "_" + i + ".pdb"
                ppdb.to_pdb(path = (self.ruta + "/" + nom),
                        records = [i],
                        gz = False,
                        append_newline = True)
        
        # Imagen PDB
        pymol.cmd.load(self.ruta + "/" + self.nombreArchivo, self.nombreArchivo)
        pymol.cmd.disable("all")
        pymol.cmd.enable(self.nombreArchivo)
        print(pymol.cmd.get_names())
        pymol.cmd.hide("all")
        pymol.cmd.show("cartoon")
        pymol.cmd.set("ray_opaque_background", 0)
        pymol.cmd.pretty(self.nombreArchivo)
        pymol.cmd.png("%s.png" % (self.nombreArchivo))
        pymol.cmd.quit()


    def botonCerrar(self, btn=None):
        self.window.destroy()

    
if __name__ == "__main__":
    ventana = wnDirectorio()
    Gtk.main()
