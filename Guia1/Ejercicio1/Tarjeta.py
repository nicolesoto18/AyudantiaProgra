#!/usr/bin/env python3
# -*- coding: utf-8 -*-


class Tarjeta(): # Definición de la clase

    def __init__(self, nombre, saldo): # Definición del construcctor
        self._nombre = nombre
        self.saldo = saldo

    
    def set_nombre(self, nombre):
        self.nombre = nombre

    def get_nombre(self):
        return self.nombre

    def set_saldo(self, saldo):
        self.saldo = saldo

    def get_saldo(self):
        return self.saldo

    
    def compra(self):
        valor_compra = int(input("Ingrese el valor de la compra: "))
        self.saldo = self.saldo - valor_compra

