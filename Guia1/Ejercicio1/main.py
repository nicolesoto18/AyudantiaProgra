#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Tarjeta import Tarjeta

if __name__ == "__main__":
    
    nombre = input("Ingrese su nombre: ")
    saldo = int(input("Ingrese el saldo de su tarjeta: "))

    tarjeta1 = Tarjeta(nombre, saldo)
    tarjeta1.set_nombre(nombre)
    tarjeta1.set_saldo(saldo)
    tarjeta1.compra()
    
    print("El saldo de la tarjeta de {0} es {1}".format(tarjeta1.get_nombre(), tarjeta1.get_saldo()))
